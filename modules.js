module.exports = {
   Discord: require(`discord.js`),
   fs: require(`fs`),
   mongoose: require(`mongoose`),
   random_string: require(`crypto-random-string`)
};
