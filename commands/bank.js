module.exports = class {
   constructor() {
      this.name = "bank";
      this.alias = [];
      this.usage = "";
   }

   async run(client, message, args) {
      if (message.member.roles.cache.has("627933604378312705") && args.length > 1) {
         if (args[1] == "add") {
            if (!args[2]) return;
            if (!args[3]) return;
            let user = message.mentions.users.first();
            let amount = parseFloat(Number(args[3]).toFixed(2));
            let note = message.content.slice(
               args[0].length +
                  args[1].length +
                  args[2].length +
                  args[3].length +
                  4
            );
            client.models.userBanks.findOne(
               {
                  user_id: user.id
               },
               (err, db) => {
                  if (err) return console.error(err);
                  if (!db)
                     return message.channel.send(
                        `:x: User has no bank account.`
                     );
                  db.balance += Number(amount.toFixed(2));
                  db.history.push({
                     transaction_id: client.modules.random_string({
                        length: 10
                     }),
                     type: "SALE",
                     direction: "+",
                     amount: Number(amount.toFixed(2)),
                     timestamp: new Date(),
                     data: {
                        note: note
                     }
                  });
                  db.save(err => {
                     if (err) return console.error(err);
                     message.channel.send(
                        `:white_check_mark: Added \`${amount}\` to ${
                           user.username
                        }${user.username.endsWith("s") ? `'` : `'s`} balance.`
                     );
                  });
               }
            );
         }
         if (args[1] == "del" || args[1] == "remove" || args[1] == "delete") {
            if (!args[2]) return;
            if (!args[3]) return;
            let user = message.mentions.users.first();
            let amount = parseFloat(Number(args[3]).toFixed(2));
            let note = message.content.slice(
               args[0].length +
                  args[1].length +
                  args[2].length +
                  args[3].length +
                  4
            );
            client.models.userBanks.findOne(
               {
                  user_id: user.id
               },
               (err, db) => {
                  if (err) return console.error(err);
                  if (!db)
                     return message.channel.send(
                        `:x: User has no bank account.`
                     );
                  db.balance -= Number(amount.toFixed(2));
                  db.history.push({
                     transaction_id: client.modules.random_string({
                        length: 10
                     }),
                     type: "WITHDRAW",
                     direction: "-",
                     amount: Number(amount.toFixed(2)),
                     timestamp: new Date(),
                     data: {
                        note: note
                     }
                  });
                  db.save(err => {
                     if (err) return console.error(err);
                     message.channel.send(
                        `:white_check_mark: Removed \`$${amount}\` from ${
                           user.username
                        }${user.username.endsWith("s") ? `'` : `'s`} balance.`
                     );
                  });
               }
            );
         }
      }
      if (args[1] == "create") {
         if (message.member.roles.cache.has("627933604378312705")) {
            if (args[2] && args[3]) {
               let newdb = new client.models.userBanks({
                  user_id: message.mentions.users.first().id,
                  balance: 0,
                  email: args[3]
               });
               newdb.save(err => {
                  if (err) return console.error(err);
                  message.channel.send(
                     `:white_check_mark: Created bank account.`
                  );
               });
            }
         }
      } else {
         client.models.userBanks.findOne(
            {
               user_id: message.author.id
            },
            (err, db) => {
               if (err) return console.error(err);
               if (!db) {
                  message.channel.send(
                     new client.modules.Discord.MessageEmbed()
                        .setColor(message.guild.me.displayHexColor)
                        .setDescription(
                           `You do not have a bank account on Hideout! :slight_frown:`
                        )
                  );
                  return;
               }
               message.channel
                  .send(
                     new client.modules.Discord.MessageEmbed()
                        .setColor(message.guild.me.displayHexColor)
                        .setTitle(`Your Bank:`)
                        .setThumbnail(message.author.avatarURL())
                        .setDescription(
                           `**‎⠀**\nYour Balance: \`$${parseFloat(
                              db.balance
                           ).toFixed(
                              2
                           )}\`\n**⠀**\n🇦 Withdraw from your balance\n🇧 View your account history\n**⠀**`
                        )
                  )
                  .then(msg => {
                     let emojis = ["🇦", "🇧", "❌"];
                     for (const e of emojis) msg.react(e);
                     let collector = new client.modules.Discord.ReactionCollector(
                        msg,
                        (reaction, user) =>
                           emojis.includes(reaction.emoji.name) &&
                           user.id == message.author.id,
                        {}
                     );
                     collector.on("collect", (reaction, user) => {
                        collector.stop();
                        if (reaction.emoji.name == emojis[2]) {
                           msg.delete();
                           message.delete();
                           return;
                        }
                        if (reaction.emoji.name == emojis[0]) {
                           let emissions = message.guild.members.cache.get(
                              "201095756784992256"
                           ).user;
                           msg.reactions.removeAll();
                           msg.edit(
                              new client.modules.Discord.MessageEmbed()
                                 .setColor(message.guild.me.displayHexColor)
                                 .setTitle(`Withdraw from your Bank:`)
                                 .setThumbnail(message.author.avatarURL())
                                 .setDescription(
                                    `**⠀**\nYour Balance: \`$${parseFloat(
                                       db.balance
                                    ).toFixed(
                                       2
                                    )}\`\n**⠀**\n**How much would you like to withdraw from your balance?**\n**⠀**`
                                 )
                           );
                           let collector = new client.modules.Discord.MessageCollector(
                              message.channel,
                              m => m.author.id == message.author.id,
                              {}
                           );
                           collector.on("collect", amount => {
                              if (
                                 parseFloat(Number(amount.content).toFixed(2)) >
                                 db.balance
                              ) {
                                 amount.delete();
                                 message.channel
                                    .send(
                                       `:x: You cannot withdraw an amount higher than what is your balance!`
                                    )
                                    .then(m =>
                                       setTimeout(() => m.delete(), 2500)
                                    );
                              } else {
                                 collector.stop();
                                 emissions.send(
                                    `**New Withdraw Request from <@${
                                       message.author.id
                                    }>:**\n** **\nTheir balance: \`${
                                       db.balance
                                    }\`\nWithdraw request amount: \`${Number(
                                       amount.content
                                    ).toFixed(2)}\`\nPayPal Email: \`${
                                       db.email
                                    }\``
                                 );
                                 amount.delete();
                                 msg.edit(
                                    new client.modules.Discord.MessageEmbed()
                                       .setTitle(
                                          `Processed Withdrawal Request:`
                                       )
                                       .setColor(
                                          message.guild.me.displayHexColor
                                       )
                                       .setThumbnail(message.author.avatarURL())
                                       .setDescription(
                                          `**⠀**\nYour balance: \`$${parseFloat(
                                             db.balance
                                          ).toFixed(
                                             2
                                          )}\`\nWithdraw Request Amount: \`$${Number(
                                             amount.content
                                          ).toFixed(
                                             2
                                          )}\`\nBalance after approval: \`$${(
                                             parseFloat(db.balance) -
                                             parseFloat(
                                                Number(amount.content).toFixed(
                                                   2
                                                )
                                             )
                                          ).toFixed(2)}\``
                                       )
                                 );
                              }
                           });
                        }
                     });
                  });
            }
         );
      }
   }
};
