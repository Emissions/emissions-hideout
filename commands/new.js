module.exports = class {
   constructor() {
      this.name = "new";
      this.alias = ["ticket"];
      this.usage = "";
   }

   async run(client, message, args) {
      const omsg = await message.channel.send(
         new client.modules.Discord.MessageEmbed()
            .setColor(message.guild.me.displayHexColor)
            .setDescription(
               `Creating your ticket... please wait. <:iloveit:630504342197239831>`
            )
      );
      let channels_open = 0;
      for (const channel of message.guild.channels.cache.array()) {
         if (channel.type !== "text") continue;
         if (channel.topic == message.author.id) {
            if (channel.parentID == "665581904027844658") {
               channels_open += 1;
            } else continue;
         } else continue;
      }
      if (channels_open >= 5) {
         omsg.edit(
            new client.modules.Discord.MessageEmbed()
               .setColor(message.guild.me.displayHexColor)
               .setDescription(
                  `Hey! Unfortunately, you can only have 5 tickets open at a time. :slight_frown:`
               )
         );
         return;
      }
      let ticket_id = client.functions.genNumberBetween(10000, 99999);
      const channel = await message.guild.channels.create(
         `${message.author.username}-${ticket_id}`,
         {
            type: "text",
            topic: message.author.id,
            parent: "665581904027844658",
            permissionOverwrites: [
               {
                  id: message.author.id,
                  allow: ["SEND_MESSAGES", "VIEW_CHANNEL"]
               },
               {
                  id: message.guild.roles.cache.find(
                     x => x.name == "@everyone"
                  ),
                  deny: ["VIEW_CHANNEL", "SEND_MESSAGES"]
               },
               {
                  id: "630488781757087780",
                  allow: ["SEND_MESSAGES", "VIEW_CHANNEL"]
               }
            ]
         }
      );
      channel
         .send(`<@&630488781757087780>`)
         .then(m => setTimeout(() => m.delete(), 10));
      channel.send(
         new client.modules.Discord.MessageEmbed()
            .setColor(message.guild.me.displayHexColor)
            .setTitle(`Ticket #${ticket_id}`)
            .setDescription(
               `Welcome to your ticket. We have mentioned our Support Team. They will be with you as soon as possible. While you wait, please let us know, in detail, why you have opened this ticket.`
            )
            .addField(
               `Reason:`,
               `\`\`\`${
                  message.content.slice(args[0].length + 1) == ""
                     ? `No reason provided`
                     : message.content.slice(args[0].length + 1)
               }\`\`\``
            )
      );
      omsg.edit(
         new client.modules.Discord.MessageEmbed()
            .setColor(message.guild.me.displayHexColor)
            .setDescription(
               `Created your ticket - you may view it here: <#${channel.id}> <a:verified:631183828345421844>`
            )
      );
   }
};
