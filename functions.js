module.exports = {
   formatDate: require(`./functions/formatDate`),
   genNumberBetween: require(`./functions/genNumberBetween`)
};
