const { mongoose } = require(`../modules`);
/*
types:
- SALE
- WITHDRAW
- SEND
- RECEIVE
*/
const history = new mongoose.Schema({
   transaction_id: {
      type: String,
      required: true
   },
   type: {
      type: String,
      required: true
   },
   direction: {
      type: String,
      required: true
   },
   amount: {
      type: Number,
      required: true
   },
   timestamp: {
      type: Date,
      required: true
   },
   // note
   data: {
      type: Object,
      required: false,
      default: {}
   }
});
module.exports = mongoose.model(
   `userBanks`,
   new mongoose.Schema({
      user_id: {
         type: String,
         required: true
      },
      balance: {
         type: Number,
         required: false,
         default: 0
      },
      email: {
         type: String,
         required: true
      },
      history: [history]
   })
);
