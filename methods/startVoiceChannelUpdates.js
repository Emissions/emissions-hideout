module.exports = class {
   constructor(client) {
      this.client = client;
      this.constructVariables().then(() => {
         this.member_count();
         this.freelancer_ofc_count();
      });
   }

   constructVariables() {
      return new Promise(async (resolve, reject) => {
         try {
            this.guild = await this.client.guilds.cache.get(
               this.client.storage.messageCache.guild_id
            );
         } catch (_e) {
            return reject(_e);
         } finally {
            resolve();
         }
      });
   }

   freelancer_ofc_count() {
      return new Promise(async (resolve, reject) => {
         const channel = await this.guild.channels.cache.get(
            this.client.storage.messageCache.hideout_stats.freelancer_ofc_count
         );
         try {
            let rolecount = this.guild.roles.cache
               .get("627879957279932417")
               .members.array().length;
            channel.edit({
               name: `Freelancers Open: ${rolecount}`
            });
         } catch (_e) {
            return reject(_e);
         } finally {
            return resolve();
         }
      });
   }

   member_count() {
      return new Promise(async (resolve, reject) => {
         const channel = await this.guild.channels.cache.get(
            this.client.storage.messageCache.hideout_stats.member_count
         );
         try {
            channel.edit({
               name: `Member Count: ${this.guild.memberCount}`
            });
         } catch (_e) {
            return reject(_e);
         } finally {
            return resolve();
         }
      });
   }
};
